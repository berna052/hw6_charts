﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;
using MCEntry = Microcharts.Entry;

namespace HW6Charts
{
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();

            MainPicker.Items.Add("Bar Chart");
            MainPicker.Items.Add("Donut Chart");
            MainPicker.Items.Add("Line Chart");

            MainPicker.SelectedItem = MainPicker.Items.FirstOrDefault();
        }

        private void CreateCharts(int index)
        {
            var entries = new List<MCEntry>()
            {
                new MCEntry(100)
                {
                    Label = "Data 1",
                    Color = SKColor.Parse("#234432")
                },
                new MCEntry(50)
                {
                    Label = "Data 2",
                    Color = SKColor.Parse("#2FF234")
                },
                new MCEntry(150)
                {
                    Label = "Data 3",
                    Color = SKColor.Parse("#23CCC2")
                }
            };

            if (index == 0)
            {
                var chart = new BarChart() { Entries = entries };
                Chart1.Chart = chart;
            }
            else if(index == 1)
            {
                var chart = new DonutChart() { Entries = entries };
                Chart1.Chart = chart;
            }
            else if (index == 2)
            {
                var chart = new LineChart() { Entries = entries };
                Chart1.Chart = chart;
            }
        }

        void PickerSelectedItem(object sender, System.EventArgs e)
        {
            var index = MainPicker.SelectedIndex;
            CreateCharts(index);
        }
    }
}
